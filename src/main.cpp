#include "../include/passport_scanner.hpp"

#include <iostream>

int main (int argc, char** argv)
{
  // Store the result of cin
  std::string result = "";

  // Prompt and get the HTTP address of the video feed
  std::cout << "URL link is http://192.168.xx.xxx:8080/video, fill in IP address: ";
  std::cin >> result;

  // Complete http url
  // For Raspi camera module just replace IP with raspberry IP....
  std::string url = "http://192.168." + result + ":8080/video";

  EGate::PassportScanner passportScanner(url, 800, 600);
  while(passportScanner.Update()) {}

  return 0;
}
