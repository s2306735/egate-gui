#include "../include/passport_scanner.hpp"
#include "../include/result_transmitter.hpp"

#include <hashed_passport.h>
#include <opencv2/highgui.hpp>
#include <iostream> // cerr
#include <memory> // sharedPointer

namespace EGate
{
  PassportScanner::PassportScanner(std::string url, int width, int height)
    :
    mainWindow(width, height),
    m_capture {},
    m_featureDetector {kWindowName},
    m_mrzParser {"../TrainingData"}
  {
    this->width = width;
    this->height = height;

    std::cout << "Attempting to open URL...\n";
    while (!OpenHTTPStream(url)) {}
    std::cout << "URL link successfully opened!\n";
    std::cout << "Creating Gtkmm window...\n";

    gui = new std::thread([this] { run_gui(); });
  }

  PassportScanner::~PassportScanner() {
      gui->join();
  }

  bool PassportScanner::OpenHTTPStream(std::string url)
  {
    // Return if url was successfully opened
    bool success = m_capture.open(url);

    if (success) m_capture.set(cv::CAP_PROP_BUFFERSIZE, 0);

    return success;
  }

  bool PassportScanner::CreateCVWindow()
  {
    if (!m_capture.isOpened()) return false;

    // Get video dimensions of m_capture
    double width = m_capture.get(cv::CAP_PROP_FRAME_WIDTH);
    double height = m_capture.get(cv::CAP_PROP_FRAME_HEIGHT);

    double r = kDesiredWidth / width;
    double resizeHeight = height * r;

    // Create window
    cv::namedWindow(kWindowName, cv::WINDOW_NORMAL);
    // Resize window
    cv::resizeWindow(kWindowName, kDesiredWidth, resizeHeight);

    return true;
  }

  bool PassportScanner::Update()
  {
    // Prevent the drawing if size is 0:
    if (width == 0 || height == 0) {
        return true;
    }

    if (!m_capture.read(m_frame))
    {
      std::cout << "End of frame\n";
      return false;
    }

    if (m_runVideoFeed)
    {
      //cv::imshow(kWindowName, m_frame);

      m_runVideoFeed = !m_featureDetector.DetectPassport(m_frame, &portrait, &mrz);

      if (!m_runVideoFeed) {
         //std::cout << "Trying to switch frames!\n";
         mainWindow.switch_frame(5);
      }

      resize(m_frame, output, cv::Size(width, height), 0, 0, cv::INTER_LINEAR);

      // Initializes a pixbuf sharing the same data as the mat:
      Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create_from_data(
        (guint8*)output.data,
        Gdk::COLORSPACE_RGB,
        false,
        8,
        output.cols,
        output.rows,
        (int)output.step);

      mainWindow.drawPixbuf(pixbuf);

      if (portrait.data && mrz.data)
      {
        std::string result = m_mrzParser.getMRZ(mrz);
        std::cout << result << std::endl;

        // COMMUNICATION TO SERVER NOT WORKING YET, need to fix errors
        std::unique_ptr<HashedPassport> hashedPassport = HashedPassport::fromMRZone(std::shared_ptr<cv::Mat>(new cv::Mat(portrait)), result);

        // Result success = transmit_result(*hashedPassport);

        // // std::cout << "Was scan successful? : " << success << std::endl;

        // switch (success)
        // {
        //   case Result::INVALID:
        //     std::cout << "Invalid Result!" << std::endl;
        //     break;

        //   case Result::NO_MATCH:
        //     std::cout << "No Match from Result!" << std::endl;
        //     break;

        //   case Result::OK:
        //     std::cout << "Valid Result! Please pass through." << std::endl;
        //     break;

        //   default:
        //     std::cout << "Huh" << std::endl;
        //     break;
        // }
      }

      portrait.release();
      mrz.release();
    }

    return true;
  }

  void PassportScanner::run_gui() {
      app->run(mainWindow);
  }
}
