#include "result_transmitter.hpp"
#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>
#include <regex>
#include <stdexcept>

const std::regex URL_REGEX = std::regex("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$");

int extract_response_code(std::string response)
{
  response = response.erase(0, response.find(" ") + 1);
  response.substr(0, response.find(" "));
  try
  {
    return std::stoi(response);
  }
  catch (std::invalid_argument& e)
  {
    std::cout << "ERROR: Given invalid response code '" << response << "'." << std::endl;
    return 500;
  }
}

bool extract_response_body(std::string response)
{
  response = response.erase(0, response.find("\n\n") + 2);
  if (response == "passport ok")
    return true;
  else if (response == "passport not ok")
    return false;
  std::cout << "ERROR: Invalid airport server response '" << response << "'." << std::endl;
  return false;
}

Response curl(const char* url, const std::string& json)
{
  if (!std::regex_match(url, URL_REGEX))
  {
    std::cout << "ERROR: Passed invalid airport server url '" << url << "." << std::endl;
    return {extract_response_code(""), extract_response_body("")};
  }
  std::stringstream curlCommand;
  curlCommand << "curl -X POST -H \"Content-Type: application/json\""
                 << "-d '" << json << "'"
                 << url;
  char buffer[128];
  std::string result = "";
  FILE* pipe = popen(curlCommand.str().c_str(), "r");
  if (!pipe)
  {
    std::cout << "ERROR: Could not open pipe." << std::endl;
    return {extract_response_code(""), extract_response_body("")};
  }
  while (fgets(buffer, sizeof buffer, pipe) != NULL)
    result += buffer;
  if (!(result.substr(0, 9) == "(stdin)= "))
    std::cout << "ERROR: Malformed openssl response: '" << result << "'." << std::endl;
  pclose(pipe);
  return {extract_response_code(result), extract_response_body(result)};
}

namespace EGate
{
  Result transmit_result(HashedPassport& scanned)
  {
    if (std::getenv("AIRPORT_SERVER") == nullptr)
    {
      std::cout << "ERROR: Airport server url has not been set. Please set AIRPORT_SERVER before running."
                << std::endl;
      return Result::INVALID;
    }
    auto json = scanned.getJson();
    Response res = curl(std::getenv("AIRPORT_SERVER"), json);
    if (res.responseCode != 200)
      return Result::INVALID;
    else if (!res.accepted)
      return Result::NO_MATCH;
    else
      return Result::OK;
  }
}
