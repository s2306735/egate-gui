#include <string>
#include <tesseract/baseapi.h>
#include <tesseract/genericvector.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>
#include "../MRZ_parser.cpp"

using namespace std;
using namespace cv;

int main() {
    Mat im = cv::imread("./TestData/TestMRZImage.png", IMREAD_COLOR);
    MRZ_parser parser = MRZ_parser("../TrainingData");
    cout << "Expected: P<NLDDE<BRUIJN<<WILLEKE<LISELOTTE<<<<<<<<<<<SPECI20142NLD6503101F2403096999999990<<<<<84 \n";
    cout << "Result:   " << parser.getMRZ(im);
}