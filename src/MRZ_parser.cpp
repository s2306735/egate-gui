#include <string>
#include <tesseract/baseapi.h>
#include <tesseract/genericvector.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>
#include "../include/MRZ_parser.h"
#include <clocale>

using namespace std;
using namespace cv;

namespace EGate
{
    MRZ_parser::MRZ_parser(const char *path) {
        init(path);
    }

    void MRZ_parser::init(const char *path) {
        std::setlocale(LC_ALL, "C");
        ocr = new tesseract::TessBaseAPI();
        GenericVector<STRING> pars_vec;
        pars_vec.push_back("load_system_dawg");
        pars_vec.push_back("load_freq_dawg");
        pars_vec.push_back("load_unambig_dawg");
        pars_vec.push_back("load_punc_dawg");
        pars_vec.push_back("load_number_dawg");
        pars_vec.push_back("load_number_dawg");
        pars_vec.push_back("load_bigram_dawg");
        pars_vec.push_back("tessedit_char_whitelist");

        GenericVector<STRING> pars_values;
        for (int i = 0; i < 7; i++) {
            pars_values.push_back("F");
        }
        pars_values.push_back("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ<");
        if (ocr->Init(path, "eng", tesseract::OEM_TESSERACT_ONLY, NULL, 0, &pars_vec, &pars_values, false) < 0)
          std::cout << "Error initialising MZR Parser" << std::endl;
        ocr->SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
    }

    string MRZ_parser::getMRZ(Mat MRZimage) {
        ocr->SetImage(MRZimage.data, MRZimage.cols, MRZimage.rows, 3, MRZimage.step);
        string result = string(ocr->GetUTF8Text());
        result.erase(remove_if(result.begin(), result.end(), ::isspace), result.end());
        return(result);
    }

    void MRZ_parser::close() {
        ocr->End();
    }
}
