#include "../include/camera_drawing_area.hpp"

CameraDrawingArea::CameraDrawingArea(int width, int height) : m_Mutex()
{
    this->width = width;
    this->height = height;
    // Lets refresh drawing area every now and then.
    pixbuf = Gdk::Pixbuf::create_from_file("../images/loading.jpg");
    everyNowAndThenConnection = Glib::signal_timeout()
      .connect(sigc::mem_fun(*this, &CameraDrawingArea::everyNowAndThen), 300);
}

CameraDrawingArea::~CameraDrawingArea()
{
    everyNowAndThenConnection.disconnect();
}

/**
 * Every now and then, we invalidate the whole Widget rectangle,
 * forcing a complete refresh.
 */
bool CameraDrawingArea::everyNowAndThen()
{
    auto win = get_window();
    if (win) {
        Gdk::Rectangle r(0, 0, width, height);
        win->invalidate_rect(r, false);
    }

    // Don't stop calling me:
    return true;
}

/**
 * Called every time the widget needs to be redrawn.
 * This happens when the Widget got resized, or obscured by
 * another object, or every now and then.
 */
bool CameraDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{

    // Prevent the drawing if size is 0:
    if (width == 0 || height == 0) {
        return true;
    }

    // Display
    m_Mutex.lock();
    Gdk::Cairo::set_source_pixbuf(cr, pixbuf);
    cr->paint();
    m_Mutex.unlock();

    // Don't stop calling me.
    return true;
}

void CameraDrawingArea::set_pixbuf(Glib::RefPtr<Gdk::Pixbuf> new_pixbuf) {
    m_Mutex.lock();
    pixbuf = new_pixbuf;
    m_Mutex.unlock();
}
