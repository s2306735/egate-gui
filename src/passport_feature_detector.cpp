#include "../include/passport_feature_detector.hpp"

#include <iostream>
#include <opencv2/calib3d.hpp> // findHomography
#include <opencv2/core.hpp> // perspectiveTransform

#include <opencv2/imgproc.hpp> // cvtColor, perspectiveFuncs
#include <opencv2/objdetect.hpp> // CascadeClassifier

namespace EGate
{
  // https://www.youtube.com/watch?v=KI-0yhJ_q5o
  // 
  FeatureDetector::FeatureDetector(const char* windowName)
    :
    kWindowName {windowName}
  {
  }

  bool FeatureDetector::DetectPassport(cv::Mat frame, cv::Mat *portrait, cv::Mat *mrz)
  {
    m_sceneImage = frame;

    if (BlurDetection())
    {
      return MRZExtractor(frame, mrz) && FaceExtractor(frame, portrait);
    }

    else return false;
  }

  // https://www.pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
  bool FeatureDetector::BlurDetection()
  {
    // Get grayscale image of our scene image
    cv::Mat grayScale;
    cv::cvtColor(m_sceneImage, grayScale, cv::COLOR_BGR2GRAY);

    // Get laplacian of the grayscale image
    cv::Mat lap;
    cv::Laplacian(grayScale, lap, CV_64F);

    // Get the stdDev of the laplacian, this will indicate blurriness
    cv::Mat mean, stdDev;
    cv::meanStdDev(lap, mean, stdDev);
  
    double blurriness = stdDev.at<double>(0) * stdDev.at<double>(0);

    double threshold = 170;

    // Debug, output blurriness value
    // cv::putText(m_sceneImage, std::to_string(blurriness), cv::Point(10, 100), cv::FONT_HERSHEY_COMPLEX, 2, cv::Scalar(255, 0, 0), 3);
    std::cout << "Blurriness: " << blurriness << std::endl;

    // Blurriness has to above threshold, otherwise blur detection fails
    // I dunno why its above and not below T.T
    return blurriness >= threshold;
  }

  //https://www.digitalocean.com/community/tutorials/how-to-detect-and-extract-faces-from-an-image-with-opencv-and-python
  bool FeatureDetector::FaceExtractor(cv::Mat img, cv::Mat *out)
  {
    // Convert to grayscale
    cv::Mat grayImg;
    cv::cvtColor(img, grayImg, cv::COLOR_BGR2GRAY);

    cv::CascadeClassifier faceCascade;
      
    if (!faceCascade.load("../Cascades/haarcascade_frontalface_alt.xml"))
    {
      std::cout << "Error loading face cascade!" << std::endl;
      return false;
    }

    std::vector<cv::Rect> faces;
    faceCascade.detectMultiScale(grayImg, faces, 1.3, 3, 0, cv::Size(30, 30));

    cv::Rect portrait;
    int largestArea = 0;

    if (faces.size() > 0)
    {
      for (uint64_t i = 0; i < faces.size(); ++i)
      {
        cv::Rect face = faces[i];

        if (face.area() > largestArea)
        {
          largestArea = face.area();
          portrait = face;
        }
      }

      // Because Halvor wants to see the chin
      portrait.height *= 1.1;

      std::vector<int> compression_params;
      compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
      compression_params.push_back(0);

      cv::Mat portraitImg = img(portrait);
      *out = portraitImg;
      cv::imwrite("passportportrait.png", portraitImg, compression_params);
      cv::imshow("Portrait", portraitImg);

      return true;
    }

    else return false;
  }

  bool FeatureDetector::MRZExtractor(cv::Mat img, cv::Mat *out)
  {
    float referenceHeight = 600.f;
    float scalePerc = img.cols / referenceHeight;

    cv::Mat rectKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Point(13 * scalePerc, 5 * scalePerc));
    cv::Mat sqKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Point(21 * scalePerc, 21 * scalePerc));

    // Convert to grayscale
    cv::Mat grayImg;
    cv::cvtColor(img, grayImg, cv::COLOR_BGR2GRAY);

    // Smooth image
    cv::GaussianBlur(grayImg, grayImg, cv::Point(3, 3), 0);
    cv::Mat blackhat;
    cv::morphologyEx(grayImg, blackhat, cv::MORPH_BLACKHAT, rectKernel);

    cv::Mat gradX;
    cv::Sobel(blackhat, gradX, CV_32F, 1, 0, 1);
    gradX = cv::abs(gradX);
    double minVal, maxVal;
    cv::minMaxLoc(gradX, &minVal, &maxVal);

    gradX.convertTo(gradX, CV_8U);
    gradX = ((uint8_t)255 * ((gradX - (uint8_t)minVal) / ((uint8_t)maxVal - (uint8_t)minVal)));

    // Apply closing operation using rectKernel to close gaps in between letters
    cv::morphologyEx(gradX, gradX, cv::MORPH_CLOSE, rectKernel);
    // Apply Otsu's thresholding method
    cv::Mat threshold;
    cv::threshold(gradX, threshold, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

    // Another closing operation using sqKernel to close gaps between lines of MRZ
    cv::morphologyEx(threshold, threshold, cv::MORPH_CLOSE, sqKernel);
    // Perform series of erosions to break apart connected components
    cv::erode(threshold, threshold, cv::noArray(), cv::Point(-1, -1), 4);
    
    // Find contours in threshold image and sort by size
    std::vector <std::vector <cv::Point>> contours;
    cv::findContours(threshold, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    // imutils implementation of Grab_Contours
    // std::vector<cv::Point> grabbedContours = contours.size() == 2 ? contours[0] : contours[1];

    // https://github.com/developer79433/passport_mrz_detector_cpp/blob/master/find_mrz.cpp
    std::sort(
      contours.begin(), 
      contours.end(), 
      [](const std::vector<cv::Point>& c1, const std::vector<cv::Point>& c2)
      { return cv::contourArea(c1, false) > contourArea(c2, false); }
    );

    // Loop through contours
    for (uint64_t i = 0; i < contours.size(); ++i)
    {
      std::vector<cv::Point> contour = contours[i];

      // Compute the bounding box of the contour and use it to compute
      // aspect and coverage ratio of the bounding box width to that of image
      cv::Rect box = cv::boundingRect(contour);
      double aspectRatio = (double)box.width / (double)box.height;
      double coverageWidth = (double)box.width / (double)grayImg.size().height;

      if (aspectRatio > 5 && coverageWidth > 0.75)
      {
        int pX = (int)((box.x + box.width) * 0.03);
        int pY = (int)((box.y + box.height) * 0.03);

        cv::Rect roi = cv::Rect(box.x - pX, box.y - pY, box.width + (pX * 2), box.height + (pY * 2));

        if (roi.x >= 0 && roi.y >= 0 && roi.x + roi.width < img.cols && roi.y + roi.height < img.rows)
        {
          cv::Mat mrz = img(roi);
          *out = mrz;

          //DEBUG
          cv::imshow(kWindowName, mrz);
          std::vector<int> compression_params;
          compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
          compression_params.push_back(0);
          cv::imwrite("MRZ.png", mrz, compression_params);

          return true;
        }
      }
    }

    return false;
  }
}