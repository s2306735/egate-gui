#include "main_window.hpp"
#include <iostream>

MainWindow::MainWindow(int width, int height) :
stack(),
cameraDrawingArea(width, height)
{
    this->set_default_size(width, height);
    this->set_resizable(false);
    this->set_title("E-gate");

    cameraDrawingArea.show();
    title[0].set_markup("<span size='xx-large'>Please scan your passport.</span>");
    title[0].show();
    frames[0].set_label("0");
    m_box[0].set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_box[0].pack_start(title[0], Gtk::PACK_SHRINK);
    m_box[0].pack_start(cameraDrawingArea, Gtk::PACK_EXPAND_WIDGET);
    frames[0].add(m_box[0]);
    m_box[0].show();
    frames[0].show_all();

    frames[1].set_label("1");

    frames[2].set_label("2");

    title[3].set_markup("<span size='xx-large'>Unsupported passport detected.\nPlease go to manual passport control.</span>");
    title[3].show();
    frames[3].set_label("3");
    m_box[3].set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_box[3].pack_start(title[3]);
    frames[3].add(m_box[3]);
    m_box[3].show();
    frames[3].show_all();

    title[4].set_markup("<span size='xx-large'>A problem has -occured.\nPlease wait. Our staff will help you momentarily.</span>");
    title[4].show();
    frames[4].set_label("4");
    m_box[4].set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_box[4].pack_start(title[4]);
    frames[4].add(m_box[4]);
    m_box[4].show();
    frames[4].show_all();

    title[5].set_markup("<span size='xx-large'>Scan was successful!\nPlease pass through the gate.</span>");
    title[5].show();
    frames[5].set_label("5");
    m_box[5].set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_box[5].pack_start(title[5]);
    frames[5].add(m_box[5]);
    m_box[5].show();
    frames[5].show_all();

    for (int i = 0; i < MAX_FRAMES; i++)
      stack.add(frames[i]);

    stack.set_visible_child(frames[0]);
    add(stack);
    stack.show();
}

void MainWindow::switch_frame(int frame) {
    stack.set_visible_child(frames[frame]);
}

void MainWindow::drawPixbuf(Glib::RefPtr<Gdk::Pixbuf> new_pixbuf) {
    //Gtk::Widget currFrame = stack.get_visible_child();
  //  if (!currFrame->get_label()).compare("0")) {
        cameraDrawingArea.set_pixbuf(new_pixbuf);
    //}
}
