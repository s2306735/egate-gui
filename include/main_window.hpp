#include <gtkmm.h>
#include <thread>
#include "camera_drawing_area.hpp"
#define MAX_FRAMES 6

class MainWindow : public Gtk::Window {
public:
    MainWindow(int width, int height);
    virtual ~MainWindow() = default;
    void switch_frame(int frame);
    void drawPixbuf(Glib::RefPtr<Gdk::Pixbuf> new_pixbuf);
private:
    Gtk::Frame frames[MAX_FRAMES];
    Gtk::Stack stack;
    //frame 0
    Gtk::Box m_box[MAX_FRAMES];
    Gtk::Label title[MAX_FRAMES];
    CameraDrawingArea cameraDrawingArea;
};
