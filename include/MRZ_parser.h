#pragma once
#ifndef MRZ_PARSER_H
#define MRZ_PARSER_H

#include <string>
#include <tesseract/baseapi.h>
#include <tesseract/genericvector.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>

namespace EGate
{
    class MRZ_parser {
    
    public:    tesseract::TessBaseAPI *ocr;
                static MRZ_parser* parser;
                void init(const char *path);
                MRZ_parser(const char *path);
                std::string getMRZ(cv::Mat MRZimage);
                void close();
    };
}



#endif