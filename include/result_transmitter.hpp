#include <hashed_passport.h>

struct Response
{
  int responseCode;
  bool accepted;
};

int extract_response_code(std::string response);
bool extract_response_body(std::string response);
Response curl(const char* url, const std::string& json);

namespace EGate
{
  enum class Result {
    OK, // Passport is good, open door
    NO_MATCH, // Passport doesn't exist, close door until security arrives
    INVALID // Passport is not compatible, open entry door and direct to manual check
  };

  Result transmit_result(HashedPassport& scanned);
}
