#include <opencv2/features2d.hpp> // ORB
#include <opencv2/highgui.hpp> // Mat

namespace EGate
{
  class FeatureDetector
  {
    public:
      // Param constructor
      FeatureDetector(const char*);

      bool DetectPassport(cv::Mat, cv::Mat*, cv::Mat*);
      bool BlurDetection();
      bool MRZExtractor(cv::Mat, cv::Mat*);
      bool FaceExtractor(cv::Mat, cv::Mat*);

    private:
      // Store the window name from passport scanner so this class can use it
      const char* kWindowName;

      // Scene image (storing for later functions)
      cv::Mat m_sceneImage;

      // Matches
      std::vector<cv::DMatch> m_matches;
      // Matches below a certain distance
      std::vector<cv::DMatch> m_goodMatches;
  };
}