#include <gtkmm.h>
#include <opencv2/highgui.hpp>
#include <mutex>

class CameraDrawingArea : public Gtk::DrawingArea {
    public:
        CameraDrawingArea(int width, int height);
        virtual ~CameraDrawingArea();
        void set_pixbuf(Glib::RefPtr<Gdk::Pixbuf> new_pixbuf);

    protected:
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
        bool everyNowAndThen();

    private:
        sigc::connection everyNowAndThenConnection;
        int width, height;
        Glib::RefPtr<Gdk::Pixbuf> pixbuf;
        mutable std::mutex m_Mutex;
};
