#pragma once

#include "main_window.hpp"
#include "opencv2/videoio.hpp"  // cv::Mat
#include "../include/passport_feature_detector.hpp"
#include "../include/MRZ_parser.h"
#include <gtkmm.h>

#include <string>
#include <thread>

namespace EGate
{
  class PassportScanner
  {
    public:
      // Default Constructor
      PassportScanner(std::string url, int width, int height);
      virtual ~PassportScanner();
      // Opens the inputted HTTP url
      bool OpenHTTPStream(std::string);
      // Initialise the window that will show the video feed
      bool CreateCVWindow();
      // Read from video and show it on the window
      bool Update();

    private:
      // Name of the window
      const char* kWindowName = "E-gate";
      // The width to resize the video feed to
      const int kDesiredWidth = 1080;
      int width, height;

      // Holds the current frame of the video/stream
      cv::Mat m_frame;
      cv::Mat output;
      // Whether to keep showing the video feed
      bool m_runVideoFeed = true;

      Glib::RefPtr<Gtk::Application> app = Gtk::Application::create("org.checkly.egate");
      MainWindow mainWindow;
      std::thread *gui;

      // Holds the results of passport scanner
      cv::Mat portrait, mrz;

      // Holds the video feed
      cv::VideoCapture m_capture;
      // Feature Detector object
      FeatureDetector m_featureDetector;
      // MRZ Parser object
      MRZ_parser m_mrzParser;

      void SaveFrame();
      void run_gui();

      // void ReadInput();
  };
}
