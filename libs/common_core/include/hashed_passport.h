#pragma once

#include <memory>
#include <string>
#include "opencv4/opencv2/core/mat.hpp"

typedef std::shared_ptr<cv::Mat> Image;

class HashedPassport {
 public:
  static std::unique_ptr<HashedPassport> fromMRZone(const Image portrait, std::string mr_zone);
  static std::unique_ptr<HashedPassport> withMRZHash(std::string countryCode,
                                                     std::string passportID,
                                                     const Image portrait,
                                                     std::string mr_zone);
  static std::string hash(std::string digest);
  std::string getDocumentNumber();
  std::string getCountryCode();
  Image getPortrait();
  std::string getMachineReadableZone();
  std::string getJson();

 private:
  HashedPassport(std::string document_number,
                 std::string country_code,
                 Image portrait,
                 std::string machine_readable_zone);
  std::string document_number;
  std::string country_code;
  Image portrait;
  std::string machine_readable_zone;
};
