#pragma once

#include <string>
#include <opencv2/opencv.hpp>

const std::string base64Characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz0123456789+/";
bool isBase64(char c);
std::string base64Decode(std::string const& encodedString);
cv::Mat stringToMat(std::string s);
