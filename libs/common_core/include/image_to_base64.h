#include <string>

const std::string base64Characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz0123456789+/";
std::string base64Encode(char const* bytesToEncode, int length);